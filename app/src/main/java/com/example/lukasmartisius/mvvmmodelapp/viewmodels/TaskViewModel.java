package com.example.lukasmartisius.mvvmmodelapp.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.lukasmartisius.mvvmmodelapp.BR;
import com.example.lukasmartisius.mvvmmodelapp.model.Task;
import com.example.lukasmartisius.mvvmmodelapp.model.TaskList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TaskViewModel extends BaseObservable {
    private TaskList taskList;
    String name, description;
    ArrayList<Map<String,String>> myList = new ArrayList<Map<String,String>>();
    private int ID = 0;

    public TaskViewModel()
    {
        taskList = new TaskList();
    }

    public void refreshMappedList()
    {
        myList.clear();
        for(int i =0; i < taskList.getTaskListLength(); i++) {
            Map<String,String> listItemMap = new HashMap<String,String>();
            listItemMap.put("title", taskList.getTask(i).getName());
            listItemMap.put("description", taskList.getTask(i).getDescription());
            myList.add(listItemMap);
        }
    }

    public ArrayList<Map<String,String>> getMyList()
    {
        return myList;
    }

    public ArrayList<Map<String,String>> abc()
    {
        return myList;
    }

    public void addNewTask(String name, String description)
    {
        taskList.addTask(new Task(ID, name, description));
        refreshMappedList();
        ID++;
    }

    public void afterNameTextChanged(CharSequence s) {
        name = s.toString();
    }

    public void afterDescriptionTextChanged(CharSequence s) {
        description = s.toString();
    }

}
