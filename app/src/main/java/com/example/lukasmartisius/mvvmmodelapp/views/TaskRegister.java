package com.example.lukasmartisius.mvvmmodelapp.views;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lukasmartisius.mvvmmodelapp.R;
import com.example.lukasmartisius.mvvmmodelapp.databinding.ActivityMainBinding;
import com.example.lukasmartisius.mvvmmodelapp.databinding.ActivityTaskRegisterBinding;
import com.example.lukasmartisius.mvvmmodelapp.viewmodels.TaskViewModel;

public class TaskRegister extends AppCompatActivity {
    private static Context mContext;
    ActivityTaskRegisterBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        binding = DataBindingUtil.setContentView(this, R.layout.activity_task_register);
        binding.setViewModel(new TaskViewModel());
        binding.executePendingBindings();
    }


    public void saveClick(View v)
    {
        Intent resultData = new Intent();
        resultData.putExtra("name", binding.taskName.getText().toString());
        resultData.putExtra("description", binding.taskDescription.getText().toString());
        setResult(RESULT_OK, resultData);
        finish();
    }

}
