package com.example.lukasmartisius.mvvmmodelapp.views;

import android.app.Activity;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.lukasmartisius.mvvmmodelapp.databinding.ActivityMainBinding;


import com.example.lukasmartisius.mvvmmodelapp.R;
import com.example.lukasmartisius.mvvmmodelapp.viewmodels.TaskViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    SimpleAdapter simpleAdapter;
    ActivityMainBinding binding;
    TaskViewModel taskViewModel = new TaskViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(new TaskViewModel());
        binding.executePendingBindings();

        makeSetup();
    }

    public void addTask(View v)
    {
        Intent intent = new Intent(this, TaskRegister.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_CANCELED) {
            // code to handle cancelled state
        }
        else if (requestCode == 1) {
            // code to handle data from TaskRegister
            taskViewModel.addNewTask(data.getStringExtra("name"), data.getStringExtra("description"));
            simpleAdapter.notifyDataSetChanged();
        }
    }

    public void makeSetup()
    {
        simpleAdapter = new SimpleAdapter(this,taskViewModel.abc(),android.R.layout.simple_list_item_2,
                new String[]{"title","description"},new int[]{android.R.id.text1,android.R.id.text2});

        binding.tasksContainer.setAdapter(simpleAdapter);

        //addClickForItem();
    }

    public void addClickForItem(ListView lv)
    {
                binding.tasksContainer.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                  {
                                      @Override
                                      public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
                                      {
                                          /*str.add("position: " + position);
                                          adapter.notifyDataSetChanged();*/
                                      }}
        );
    }
}
